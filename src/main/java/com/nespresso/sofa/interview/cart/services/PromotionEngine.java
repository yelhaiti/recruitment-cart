package com.nespresso.sofa.interview.cart.services;

import java.util.Map;

import com.nespresso.sofa.interview.cart.model.Cart;

/**
 * If one or more product with code "1000" is purchase, ONE product with code
 * 9000 is offer For each 10 products purchased a gift with code 7000 is offer.
 */
public class PromotionEngine {

	private static final int Gift_minimu_Products_quantity = 10;
	private static final int Max_Promotion_Quantity = 1;
	public static final String PROMOTION = "9000";
	public static final String PRODUCT_WITH_PROMOTION = "1000";
	public static final String GIFT = "7000";

	private CartService cartService = new CartService();

	public Cart apply(Cart cart) {
		Map<String, Integer> products = cart.getProducts();
		if (ShouldNotAddPromotion(products)) {
			products.remove(PROMOTION);
		}
		if (ShouldAddPromotion(products)) {
			products.put(PROMOTION, Max_Promotion_Quantity);
		}
		if (ShouldAddGift(products)) {
			products.put(PROMOTION, getGeftQuantity(products));
		}
		return new Cart(products);
	}

	private Integer getGeftQuantity(Map<String, Integer> products) {
		return totalProduct(products) / Gift_minimu_Products_quantity;
	}

	private boolean ShouldAddGift(Map<String, Integer> products) {
		if (totalProduct(products) >= Gift_minimu_Products_quantity) {
			return true;
		}
		return false;
	}

	private int totalProduct(Map<String, Integer> products) {
		int total = 0;
		for (Integer productsCount : products.values()) {
			total+=productsCount;
		}
		return total;
	}

	private boolean ShouldAddPromotion(Map<String, Integer> products) {
		return !products.containsKey(PROMOTION) && products.containsKey(PRODUCT_WITH_PROMOTION);
	}

	private boolean ShouldNotAddPromotion(Map<String, Integer> products) {
		return products.containsKey(PROMOTION) && !products.containsKey(PRODUCT_WITH_PROMOTION);
	}
}
