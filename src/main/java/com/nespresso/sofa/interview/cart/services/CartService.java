package com.nespresso.sofa.interview.cart.services;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.nespresso.sofa.interview.cart.model.Cart;

public class CartService {

    private static final int Minimum_Quantity_To_Add = 1;

	@Autowired
    private PromotionEngine promotionEngine;

    @Autowired
    private CartStorage cartStorage;

    /**
     * Add a quantity of a product to the cart and store the cart
     *
     * @param cartId
     *     The cart ID
     * @param productCode
     *     The product code
     * @param quantity
     *     Quantity must be added
     * @return True if card has been modified
     */
    public boolean add(UUID cartId, String productCode, int quantity) {
    	if(isNotEligibleToAdd(quantity)) {
    		return false;
    	}
        final Cart cart = cartStorage.loadCart(cartId);
        cart.addProduct(productCode,quantity);
        cartStorage.saveCart(cart);
        return true;
    }

    private boolean isNotEligibleToAdd(int quantity) {
		return quantity < Minimum_Quantity_To_Add;
	}

	/**
     * Set a quantity of a product to the cart and store the cart
     *
     * @param cartId
     *     The cart ID
     * @param productCode
     *     The product code
     * @param quantity
     *     The new quantity
     * @return True if card has been modified
     */
    public boolean set(UUID cartId, String productCode, int quantity) {
        final Cart cart = cartStorage.loadCart(cartId);
        int existantQuantity = cart.getExistingProductQuantity(productCode);
        if(shouldNotSet(existantQuantity, quantity)) {
        	return false;
        }
        cart.SetProduct(productCode,quantity);
        cartStorage.saveCart(cart);
        return true;
    }

    private boolean shouldNotSet(int existantQuanity, int newQunatity) {
		return existantQuanity==newQunatity;
	}

	/**
     * Return the card with the corresponding ID
     *
     * @param cartId
     * @return
     */
    public Cart get(UUID cartId) {
        return cartStorage.loadCart(cartId);
    }
}
