package com.nespresso.sofa.interview.cart.model;

import static java.util.UUID.randomUUID;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.test.util.ReflectionTestUtils;

public final class Cart implements Serializable {

    private final UUID id;
    private Map<String,Integer> products = new HashMap<>();;

    public Cart() {
        this(randomUUID());
       
    }

    public Cart(UUID id) {
        this.id = id;
    }

    public Cart(Map<String, Integer> products) {
        this.id = randomUUID();
        this.products=products;
    }

    public UUID getId() {
        return id;
    }

    public Map<String, Integer> getProducts() {
        return new HashMap<>(products);
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Cart {id: ");
		builder.append(id);
		builder.append(", products: ");
		for (Map.Entry<String, Integer> entry : products.entrySet()) {
			builder.append("{");
			builder.append(entry.getKey());
			builder.append("=");
			builder.append(entry.getValue());
			builder.append("}");
		}
		builder.append("}");
		return builder.toString();
	}

	public void addProduct(String productCode, int quantity) {
		int totalQuantity = getExistingProductQuantity(productCode)+quantity;
		products.put(productCode, totalQuantity);
	}
	public Integer getExistingProductQuantity(String productCode) {
		if(isProductInCart(productCode)) {
			return products.get(productCode);
			}
		return 0;
	}

	public boolean isProductInCart(String productCode) {
		return this.products.containsKey(productCode);
	}

	public void SetProduct(String productCode, int quantity) {
		if(isProductInCart(productCode)) {
			if(ShouldRemoveFromCart(quantity)) {
				this.removeProduct(productCode);
				return;
			}
		}
		this.products.put(productCode, quantity);
	}
	public void removeProduct(String productCode) {
		this.products.remove(productCode);
		
	}

	private boolean ShouldRemoveFromCart(int quantity) {
		return quantity <= 0;
	}

	public boolean isProductNotInCart(String productCode) {
		return !isProductInCart(productCode);
	}

	
    

}
